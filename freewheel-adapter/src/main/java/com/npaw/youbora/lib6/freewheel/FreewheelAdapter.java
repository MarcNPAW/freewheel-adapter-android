package com.npaw.youbora.lib6.freewheel;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;

import java.util.HashMap;

import tv.freewheel.ad.interfaces.IAdContext;
import tv.freewheel.ad.interfaces.IConstants;
import tv.freewheel.ad.interfaces.IEvent;
import tv.freewheel.ad.interfaces.IEventListener;
import tv.freewheel.ad.interfaces.ISlot;

/**
 * Created by Enrique on 22/01/2018.
 */

public class FreewheelAdapter extends PlayerAdapter<IAdContext> implements IEventListener{

    private IConstants fwConstants = null;

    private String[] events;

    private ISlot currentSlot;

    private double lastPlayhead;

    public FreewheelAdapter(IAdContext player) {
        super(player);
        fwConstants = getPlayer().getConstants();
        events = new String[42];
        registerListeners();
    }

    @Override
    public void registerListeners() {
        super.registerListeners();

        initializeEventArray();

        for (String event : events) {
            getPlayer().addEventListener(event, this);
        }
    }

    @Override
    public void run(IEvent iEvent) {
        String event = iEvent.getType();
        HashMap<String, Object> eventData = iEvent.getData();
        YouboraLog.debug("Freewheel event: " + event);
        if (fwConstants != null) {
            //Ignore in case of not being a pre, mid or post roll
            if(currentSlot != null){
                if(currentSlot.getTimePositionClass() != fwConstants.TIME_POSITION_CLASS_PREROLL()
                        && currentSlot.getTimePositionClass() != fwConstants.TIME_POSITION_CLASS_POSTROLL()
                        && currentSlot.getTimePositionClass() != fwConstants.TIME_POSITION_CLASS_MIDROLL()){
                    return;
                }
            }


            if(event.equals(fwConstants.EVENT_SLOT_STARTED())){
                currentSlot = getPlayer().getSlotByCustomId((String)iEvent.getData().get(fwConstants.INFO_KEY_CUSTOM_ID()));
                fireStart();
            }

            if(event.equals(fwConstants.EVENT_SLOT_ENDED())){
                fireStop();
            }

            if(event.equals(fwConstants.EVENT_AD_IMPRESSION())){
                if(!getFlags().isStarted()){
                    fireStart();
                }
                fireJoin();
            }

            if(event.equals(fwConstants.EVENT_AD_PAUSE())){
                lastPlayhead = getPlayhead();
                firePause();
            }

            if(event.equals(fwConstants.EVENT_AD_RESUME())){
                fireResume(new HashMap<String, String>(){{
                    put("adPlayhead",String.valueOf(lastPlayhead));
                }});
            }

            if(event.equals(fwConstants.EVENT_AD_CLICK())){
                fireClick();
            }

            if(event.equals(fwConstants.EVENT_AD_IMPRESSION_END())){
                fireStop();
            }

            if(event.equals(fwConstants.EVENT_AD_BUFFERING_START())){
                if(!getFlags().isStarted()){
                    fireStart();
                }
                fireBufferBegin();
            }

            if(event.equals(fwConstants.EVENT_AD_BUFFERING_END())){
                fireBufferEnd();
            }

            if(event.equals(fwConstants.EVENT_ERROR()) && getFlags().isStarted()){
                fireError(fwConstants.INFO_KEY_ERROR_INFO(),fwConstants.INFO_KEY_ERROR_CODE(),null);
            }
        }
    }

    @Override
    public void unregisterListeners() {

        for (String event : events) {
            getPlayer().removeEventListener(event, this);
        }

        super.unregisterListeners();

    }

    @Override
    public AdPosition getPosition() {

        if (currentSlot == null) return AdPosition.UNKNOWN;

        if(currentSlot.getTimePositionClass() == fwConstants.TIME_POSITION_CLASS_PREROLL()){
            return AdPosition.PRE;
        }
        if(currentSlot.getTimePositionClass() == fwConstants.TIME_POSITION_CLASS_POSTROLL()){
            return AdPosition.POST;
        }
        if(currentSlot.getTimePositionClass() == fwConstants.TIME_POSITION_CLASS_MIDROLL()){
            return AdPosition.MID;
        }

        return AdPosition.UNKNOWN;
    }

    @Override
    public Double getPlayhead() {
        return currentSlot == null ? super.getPlayhead() : currentSlot.getPlayheadTime();
    }

    @Override
    public Double getDuration() {
        return currentSlot == null ? super.getDuration() : currentSlot.getTotalDuration();
    }

    @Override
    public String getTitle() {
        if (currentSlot == null) {
            return super.getTitle();
        }
        return "FreeWheel slotId: " + currentSlot.getCustomId();
    }

    @Override
    public String getPlayerName() {
        return "FreeWheel";
    }

    @Override
    public String getVersion() {
        return BuildConfig.VERSION_NAME + "-FreeWheel";
    }

    //Necessary utility methods
    private void initializeEventArray(){
        events[0] = fwConstants.EVENT_REQUEST_INITIATED();

        events[1] = fwConstants.EVENT_REQUEST_COMPLETE();

        events[2] = fwConstants.EVENT_REQUEST_CONTENT_VIDEO_PAUSE();

        events[3] = fwConstants.EVENT_REQUEST_CONTENT_VIDEO_RESUME();

        events[4] = fwConstants.EVENT_SLOT_STARTED();

        events[5] = fwConstants.EVENT_SLOT_ENDED();

        events[6] = fwConstants.EVENT_SLOT_PRELOADED();

        events[7] = fwConstants.EVENT_USER_ACTION_NOTIFIED();

        events[8] = fwConstants.EVENT_ACTIVITY_STATE_CHANGED();

        events[9] = fwConstants.EVENT_EXTENSION_LOADED();

        events[10] = fwConstants.EVENT_SLOT_IMPRESSION();

        events[11] = fwConstants.EVENT_AD_IMPRESSION();

        events[12] = fwConstants.EVENT_AD_IMPRESSION_END();

        events[13] = fwConstants.EVENT_AD_QUARTILE();

        events[14] = fwConstants.EVENT_AD_FIRST_QUARTILE();

        events[15] = fwConstants.EVENT_AD_MIDPOINT();

        events[16] = fwConstants.EVENT_AD_THIRD_QUARTILE();

        events[17] = fwConstants.EVENT_AD_COMPLETE();

        events[18] = fwConstants.EVENT_AD_CLICK();

        events[19] = fwConstants.EVENT_AD_MUTE();

        events[20] = fwConstants.EVENT_AD_UNMUTE();

        events[21] = fwConstants.EVENT_AD_COLLAPSE();

        events[22] = fwConstants.EVENT_AD_EXPAND();

        events[23] = fwConstants.EVENT_AD_PAUSE();

        events[24] = fwConstants.EVENT_AD_RESUME();

        events[25] = fwConstants.EVENT_AD_REWIND();

        events[26] = fwConstants.EVENT_AD_ACCEPT_INVITATION();

        events[27] = fwConstants.EVENT_AD_CLOSE();

        events[28] = fwConstants.EVENT_AD_MINIMIZE();

        events[29] = fwConstants.EVENT_AD_LOADED();

        events[30] = fwConstants.EVENT_AD_STARTED();

        events[31] = fwConstants.EVENT_AD_STOPPED();

        events[32] = fwConstants.EVENT_AD_BUFFERING_START();

        events[33] = fwConstants.EVENT_AD_BUFFERING_END();

        events[34] = fwConstants.EVENT_AD_MEASUREMENT();

        events[35] = fwConstants.EVENT_AD_VOLUME_CHANGED();

        events[36] = fwConstants.EVENT_ERROR();

        events[37] = fwConstants.EVENT_RESELLER_NO_AD();

        events[38] = fwConstants.EVENT_TYPE_CLICK_TRACKING();

        events[39] = fwConstants.EVENT_TYPE_IMPRESSION();

        events[40] = fwConstants.EVENT_TYPE_CLICK();

        events[41] = fwConstants.EVENT_TYPE_STANDARD();
    }

    @Override
    public void fireStop() {
        fireStop(new HashMap<String, String>(){
            {put("adPlayhead",String.valueOf(getDuration() == null ? 0.0 : getDuration()));}
        });
    }
}