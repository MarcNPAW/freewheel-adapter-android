# Freewheel Ad Adapter

## Before starting
- This is an Ad adapter integration guide. If you want a player adapter and plugin integration guide you can find it here
- Before starting, please check out our Environments & Limitations section.

## Quick start
Displayed below you will find the steps you must follow to integrate a YOUBORA adapter.

### Using gradle
#### Install the youbora library and the related adapter

Add this yo your gradle repositories
```groovy
repositories {
    ...
    maven {
        url  "http://dl.bintray.com/npaw/youbora"
    }
    ...
}
```
Add this to your repositories on build.gradle dependencies
```groovy
repositories {
    dependencies {
        ...
        compile 'com.npaw.youbora.adapters:ima-adapter:6.0.1'
        ...
    }
}
```

### Adapter initialization
Adapters will translate ad players events to Youbora events so the plugin can understand them.
We recommend to set the adAdapter as soon as you have Freewheel context (on the example below a 
boolean is use to avoid reinstantiating adapter, and init since we do not have any player adapter)

```java
if(!youboraInitiated){
			youboraPlugin.fireInit();
			youboraPlugin.setAdsAdapter(new FreewheelAdapter(fwContext));
			youboraInitiated = true;
		}
```

### Init monitoring
You can send an /adInit from the adapter itself, in case you need to notifiy the ad start before having the metada ready.
```java
youboraPlugin.getAdsAdapter().fireAdInit();
```

### Start monitoring
The adapter will send AdStart “automatically” when it detects an Ad start, anyway there is the 
possibility to send ad start manually calling the method below:
```java
youboraPlugin.getAdsAdapter().fireStart();
```

### Stop monitoring
The adapter will send AdStop "automatically" when it detects an Ad ended, anyway there is the 
possibility to send ad stop manually calling the method below:
```java
youboraPlugin.getAdsAdapter().fireStop();
```

## Supported environments &amp; limitations
This section lists which scenarios have been certified by the NPAW team, including any existing limitations. 
Generally, NPAW provides active support for the OS versions that have an 80% of the usage share, and 
passive support for other versions with at least a 5%.
For a list of statistics relating to device and operating system adoption, view these Mix Panel trends or check the Android Developers Dashboards.

#### Supported environments
If the following scenarios do not match with your existing implementation, please get in touch with 
the NPAW team. Additionally, when reaching us, it will be very helpful if you could provide a testing 
example with the plugin integrated, so we can evaluate its behavior.

#### Player / SDK version

| Freewheel SDK |
|---------------|
| 6.20          |

#### OS Version

| Active support |
| :------------: |
| 7.1 |
| 6.0 |
| 5.0 5.1 |

| Passive support |
| ----- |
| 4.1, 4.2, 4.3, 4.4 |

#### Streaming protocol &amp; DRM
The YOUBORA plugin supports the most common protocols and is DRM-agnostic.